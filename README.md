# MINISCHEME


## Méthodologie de travail

Branche `MASTER` ? N'y pense même pas !

La branche `Master` est utilisée uniquement pour la version finale, version qui sera remise au professeur.

Pour travailler sur de nouveaux "feature", créer une nouvelle branche à partir de `dev`.

```bash
git checkout -b <feature_branch> dev
```

Une fois les ajouts et/ou modifications terminés, un pull request est requis sur github.
The master branch is only there to host final version for the teacher

<br>

## Requirements

- Java 1.11

- Gradle 5.2.1

- ChromeDriver

<br>

## Déploiement du Projet

***Note*** : Lire l'ensemble des étapes avant de procéder.

Dans un premier temps :

Se déplacer vers la racine du projet
```
$ cd mini-scheme-0/minischeme
```

1. Pour exécuter un **build**.
    
    ```
    $ gradle build
    ````
    Tous les tests seront exécutés lors du build à l'exception des tests d'interfaces.

    ---

2. Pour exécuter les **tests**.
    
    ```
    $ gradle junitConsole
    ```
    ***À noter*** : La tâche de tests requiert que l'interface graphique soit lancé. Pour se faire, simplement exécuter la commande `gradle bootRun`.  Un log des tests sera affiché sur le terminal.

    La tâche de tests sera un échec si l'interface web n'est pas "Up & Running".

    Les `tests d'interface` sont exécuté par le framework `Selenium`.

    ---
3. Pour exécuter **l'analyse de code**.

    Nous utilisons le framework `SonarQube` pour l'analyse de notre code.
    ```
    $ gradle sonarqube
    ```
    Afin de faciliter l'utilisation de SonarQube et d'être versatile, nous utilisons un serveur externe comme hôte.
    Vous trouverez le rapport à l'adresse suivante : `paris.havek.es:9000`

    ---
4. Pour exécuter **MiniScheme** en ligne de commande.

    ```
    $ gradle uberJar
    $ java -jar build/libs/minischeme-dependencies-all.jar [pathToMinischemeFile]
    ````
    Le résultat sera affiché dans le terminal utilisé.

<br>

## Interface Web

Le projet dispose d'une interface Web personnalisée à l'utilisation de MiniScheme. Il est possible d'observer le comportement du langage ailleurs qu'avec un terminal de commande.


1. Lancer l'interface Web
    ```
    $ gradle bootRun
    ```

    Le serveur web est situé à l'adresse suivante : `http://localhost:8083/`
