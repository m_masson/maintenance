package com.minischeme.web.controller;

import com.minischeme.*;
import com.minischeme.parser.api.Parser;
import java.util.List;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/")
@lombok.extern.slf4j.Slf4j
public class EvaluatorController {

    static final String COMMAND = "command";

    @GetMapping
    public String show(final Model model) {
        if (! model.containsAttribute(COMMAND)) {
            model.addAttribute(COMMAND, new RunCommand());
        }
        return "show";
    }

    @PostMapping
    public String run(
            @Valid final RunCommand command
            , final BindingResult bindingResult
            , final RedirectAttributes redirectAttributes
    ) {

        if (bindingResult.hasErrors()) {
            redirectAttributes
                    .addFlashAttribute("flash_error", "run_invalid_command_flash_error_message")
                    .addFlashAttribute(COMMAND, command)
            ;
            return "redirect:/";
        }

        try {
            final var source = command.getSource();
            log.info("CODE SOURCE RECU: <{}>", source);

            List<Object> code = Parser.parseString(source);
            final var result = new Evaluator().eval(code, GlobalEnvironment.build());
            command.setResult(String.valueOf(result));

            redirectAttributes
                    .addFlashAttribute("flash_success", "run_evaluation_flash_success_message")
                    .addFlashAttribute(COMMAND, command)
            ;
        } catch (Exception e) {
            redirectAttributes
                    .addFlashAttribute("flash_error", "run_evaluation_flash_error_message")
                    .addFlashAttribute(COMMAND, command)
            ;
        }

        return "redirect:/";
    }
}

@lombok.Data
@lombok.NoArgsConstructor
class RunCommand implements Serializable {

    @NotBlank
    @NotNull
    private String source = "";

    @NotNull
    private String result = "";
}
