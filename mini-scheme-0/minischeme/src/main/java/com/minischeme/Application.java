package com.minischeme;

import java.nio.file.*;
import java.util.*;

import com.minischeme.parser.api.Parser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static int port = 8083;

    public static void main(String... args) throws Exception {


        if (args.length == 0) {
            SpringApplication app = new SpringApplication(Application.class);
            app.setDefaultProperties(Collections
                    .singletonMap("server.port", port));
            app.run(args);
        } else {
            String contents = new String(Files.readAllBytes(Paths.get(args[0])));

            List<Object> code = Parser.parseString(contents);
            Object result = new Evaluator().eval(code, GlobalEnvironment.build());
            System.out.println(result);
        }
    }
}
