package com.minischeme;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class GlobalEnvironment {

    private GlobalEnvironment() {}

    public static Map<String, Object> build() {
        var env = new HashMap<String, Object>();

        env.put("begin", (Procedure) (List<Object> params) -> params.get(params.size() - 1));

        env.put("append", (Procedure) (List<Object> params) -> {
            var concat = new ArrayList<Object>();
            for (int i = 0; i < params.size(); i++) concat.addAll((List<Object>) params.get(i));
            return concat;
        });

        env.put("<", (Procedure) (List<Object> params) -> {
            var result = true;

            for (int i = 1 ; i < params.size() ; i++) {
                result = result && (Double) params.get(i) > (Double) params.get(i-1);
            }
            return result;

        });

        env.put("*", (Procedure) (List<Object> params) -> {
            var product = (Double) params.get(0);
            for (Object x : params.subList(1, params.size())) product *= (Double) x;
            return product;
        });

        env.put("+", (Procedure) (List<Object> params) -> {
            var result = (Double) params.get(0);
            for (Object x : params.subList(1, params.size())){
                result += (Double) x;
            }
            return result;
        });

        env.put("-", (Procedure) (List<Object> params) -> {
            var result = (Double) params.get(0);
            for (Object x : params.subList(1, params.size())) result -= (Double) x;
            return result;
        });
        // test

        env.put("and", (Procedure) (List<Object> params) -> {
            for (Object x : params) {
                if (!(boolean) x) {
                    return false;
                }
            }
            return true;
        });

        env.put("not", (Procedure) (List<Object> params) -> !(boolean) params.get(0));

        env.put("eq", (Procedure) (List<Object> params) -> {
            var result = (Double) params.get(0);
            for (Object x : params.subList(1, params.size())) {
                if (result != (double) x) {
                    return false;
                }
            }
            return true;
        });

        env.put("count", (Procedure) (List<Object> params) -> (double) ((List) params.get(0)).size());

        env.put("head", (Procedure) (List<Object> params) -> ((List) params.get(0)).get(0));

        env.put("tail", (Procedure) (List<Object> params) -> ((List) params.get(0)).subList(1, ((List) params.get(0)).size()));

        return env;
    }
}
