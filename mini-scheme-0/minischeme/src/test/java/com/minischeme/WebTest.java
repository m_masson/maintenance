package com.minischeme;

import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import com.minischeme.parser.api.Parser;

import org.junit.jupiter.api.*;

import java.io.File;
import java.io.FilenameFilter;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTest {


    private String WEB_URL = "localhost:" + Application.port;

    File dir = new File("src/main/resources/static/public/examples");
    File[] files = dir.listFiles(new FilenameFilter(){
        @Override
        public boolean accept(File dir, String name) {
            return name.toLowerCase().endsWith(".minischeme");
        }
    });

    @Test
    @DisplayName("Selenium Web Driver")
    public void TestSelenium() throws Exception {

        File chromeDriver = new File("C:\\Users\\alexi\\chromedriver\\chromedriver.exe"); // Windows Alexis' path
        boolean exists = chromeDriver.exists();

        if (!exists) {
            chromeDriver = new File("/usr/bin/chromedriver"); // Linux Armand's path
            exists = chromeDriver.exists();
        }

        if (!exists) {
            chromeDriver = new File("/usr/local/bin/chromedriver"); // Mac Maximes' path
            exists = chromeDriver.exists();
        }

        if (!exists) {
            fail();
        }

        System.setProperty("webdriver.chrome.driver", chromeDriver.toString());
        WebDriver driver = new ChromeDriver();

        driver.get(WEB_URL);
        Thread.sleep(4000);
        for (int i = 0 ; i < files.length ; i ++) {
            WebElement exec = driver.findElement(By.cssSelector("button.ui"));
            WebElement a = driver.findElement(By.cssSelector("div.dropdown"));
            a.click();
            Thread.sleep(500);
            List<WebElement> childs = a.findElements(By.xpath(".//div[@class='item']"));
            assertEquals(files.length, childs.size());
            childs.get(i).click();
            exec.click();
            Thread.sleep(2000);
        }
        driver.close();
    }

}
