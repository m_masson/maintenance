# outil d'analyse

Il existe plusieurs outils d'analyse de code source Java mais parmis les quatre fournis seulement deux d'entre eux nous semble intéressant et assez complet pour soliciter notre intérêt.

Le premier est `Sonarqube` de part la qualité de ses rapports fournis et de son interface graphique.
Le second, `CheckStyle`, au nom des plus évoquateur fait son travail efficacement.

## Sonarqube

Sonarqube est un logiciel libre offrant une mesure continu de la qualité du code source. Ce code source ne se limite pas seulement à Java mais à plus de vingt-cinq langages tel que Java, C, C++, Objective-C, C#, PHP, Flex, Groovy, JavaScript, Python, PL/SQL, COBOL. 
Il s'agit d'un logiciel actif, dernière version sortie le 20 mars 2019, et est soumis à la license publique générale limitée GNU.

## CheckStyle

De son côté, Checkstyle est lui aussi un logiciel libre offrant une mesure continu de la qualité du code source. Mais, celui-ci ne présente que, actuellement, le support du langage de programmation Java.
Ce logiciel est aussi en constante évolution, dernière version stable sortie le 30 avril 2018, et est soumis à la license LGPL.

## Comparaison des outils

Nous avons décidé de comparer ces deux outils sur 3 principaux critères. 
Nous allons étudier la Polyvalence, l'Interface Utilisateur ainsi que l'Intégration de plugins de chacun.

### Polyvalence

Un des critères que nous avons ciblé pour le choix de notre outil d’analyse statique entre Checkstyle et Sonarqube est la polyvalence. La polyvalence est la capacité à un logiciel d’être efficace dans plusieurs cas différents et qui offre plusieurs usages possibles. Selon nous, ce critère est très important car, étant présentement en apprentissage, nous cherchons un logiciel qui permettra d’accomplir toutes les tâches nécessaires au sujet de l’analyse statique de code et possiblement même pour d’autres projets.

Nous pensons que Sonarqube offre une polyvalence supérieure à Checkstyle pour les raisons suivantes. Initialement, lorsque Sonarqube a été lancé, l’outil d’analyse intégré SonarJava de Sonar n’existait pas dans le logiciel lui-même. Sonarqube incorporait plutôt les extrants de trois outils externes (Checkstyle, PMD et FindBugs) afin de faire ses rapports. À mesure que des demandes de changements et des rapports de ‘bugs’ sont arrivés, les développeurs de Sonarqube ont développé leur propre outil d’analyse disponible directement dans Sonar, sans l’utilisation de plugins externes. Cependant, ils n’ont jamais arrêté le support de ces plugins. C’est-à-dire qu’il est encore possible d’utiliser des rapports venant de Checkstyle, PMD ou bien FindBugs afin d’intégrer les données dans le rapport graphique. Cela nous donne donc plus de choix sur la provenance des données et peut faciliter l’analyse statique de code si nous venions à travailler avec une autre équipe qui n’utilise pas Sonar. Sonar, par rapport aux autres outils, offre aussi un historique des analyses et permet ainsi de voir la progression (ou la régression) des analyses statiques.

De plus, si l’utilisation de plugins externes est souhaitée, il devient possible d’obtenir des données additionnelles sur le code source analysé alors que l’utilisation de Checkstyle, par exemple, limiterait les informations des rapports aux fonctionnalitées de Checkstyle seulement. Avec Sonar, l’utilisation de plugins permettra d’avoir une meilleure vue d’ensemble du projet sous la forme d’une interface graphique facile à analyser.

### Interface Utilisateur

L'interface utilisateur est selon nous un critère très important dans l'utilisation d'un outil. C'est par l'interface utilisateur que le développeur peut naviguer et trouver l'information qu'il recherche efficacement et rapidement. Un bon interface utilisateur permet de sauver du temps au développeur et facilite aussi la compréhension des fonctionnalités utilisées par le dit outil. En ce sens, Sonarqube offre une interface utilisateur moderne et épurée. La navigation est claire, simple et organisé de façon à trouver en un clin d'oeil les statistiques recherchés. L'outil se démarque par les nombreux filtres disponibles et les nombreux menus de navigation au travers de l'application web. Nous sommes d'avis que Sonarqube est le meilleur par rapport à Checkstyle. En effet, l'interface de Sonarqube permet de filtrer l'information à l'aide de multiple filtreur, rendant possible d'afficher les statisques recherchés très rapidement et de façon isolée. De plus, on se retrouve aisément avec sa barre de menu qui nous dirige vers des sections clés de l'outil tel que les projets analysés, les règles de code propre, les problèmes classés par type (Bugs, Vulnérabilités, Code Smell et Failles de sécurité). On y retrouve aussi de multiples graphiques. Sonarqube offre à l'utilisateur la possibilité de lier plusieurs projets et d'y afficher les analyses sous une même plateforme/affichage. C'est un point très important que Checkstyle n'offre pas. Cela permet à l'utilisateur de faire des comparaisons rapidement et efficacement entre les différents projet.

Checkstyle tant qu'à lui, offre seulement une page remplit d'informations classées dans des tableaux ternes. Il est plus difficile de naviguer et de bien comprendre les erreurs car ses messages sont moins bien abrégés et donc plus complexes. L'interface est très limité et aucunement stylisée. On peut dire que l'outil est plus axé sur le contenu que l'affichage de son contenu. De plus, Checkstyle n'offre pas d'affichage commune pour plusieurs projets.

En analysant les deux outils et en s'apercevant que Sonarqube possède beaucoup plus d'aspects améliorant l'expérience de l'utilisateur par son interface, nous n'avons aucune difficulté à confirmer que l'outil est meilleur que son concurrent Checkstyle pour ce critère.

### Integration

Sonarqube est surprenant par sa capacité d'intégration et de gestion grâce aux différents outils de build tel que Maven, Gradle ou encore Ant.
Nous somme particulièrement intéressé par son analyse entièrement automatisé grâce à Gradle.

Au cours de nos recherches nous avons découvert l'outil indépendant 'Jacoco' qui nous permet de calculer le taux de couverture du code de nos tests. Ce dernier offre un rapport en format HTML une fois ajouté au fichier 'build.gradle' ainsi que quelques modifications. Lorsque nous avons découvert que le site web généré par Sonarqube au 'localhost:9000' offrait une intégration directe des rapports de 'Jacoco' sur son interface, nous savions que nous avions choisis un outil à la fois polyvalent et visuellement attrayant mais surtout un outil offrant une intégration rapide et intuitive.

Cette simplicité d'accès aux différents rapports générés par nos différents outils est un avantage non négligeable face à ses compétiteurs. Certes, CheckStyle est un outil complet et fonctionnel, mais ce dernier ne nous a pas semblé offrir une intégration d'autres outils aussi simples, tant bien même que nous doutions de la possibilité de cela sur CheckStyle.

Nous avons préféré concentrer notre temps a l'implémentation de l'outil le plus complet selon nos recherches.

La documentation de SonarQube nous offre une panoplie de 'plugins' facilement intégrable à notre environnement de développement et qui sont majoritairement des outils 'open source' développés par la communauté et modifiables selon nos désirs.

CheckStyle de son côté n'offre pas autant d'intégration d'outils ni de plateforme regroupant les différents rapports générés durant les étapes de builds ou d'exécution ! En effet, ce dernier offre une intégration directe dans 'Eclipse' ou encore 'IntelliJ' mais cela n'est qu'une intégration à un IDE et non une centralisation des rapports de données comme le fait Sonarqube.

Il est indéniable pour nous que Sonarqube présente une intégration parfaite de Jacoco ou autres outils aux rapports générés.


## Conclusion

De façon générale durant la création de ce second livrable nous avons eu un réel coup de coeur pour Sonarqube ! Il a été très difficile pour nous d'implémenter celui-ci à notre projet pour des raisons techniques diverses mais nous ne voulions pas passer à côté de cet outil offrant une multitude d'avantages par rapports aux autres outils. La qualité du site web 'localhost:9000' généré par Sonarqube nous semble largement supérieure à celle des autres outils ce qui a été un facteur de choix pour nous.
