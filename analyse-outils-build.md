# Outils de build

## Description de Gradle

Gradle est un outil de build 'open-source' qui se démarque par sa flexibilité et sa performance. Les scripts Gradle sont écrit en langage dédié, soit en 'Groovy' ou en 'Kotlin'.
On retrouve une documentation complète ainsi que des guides d'installation/utilisations complets sur leur [site officiel](https://docs.gradle.org/current/userguide/userguide.html).

Gradle supporte plusieurs IDE populaires comme Eclipse, IntelliJ, Visual Studio, Android Studio et XCode ce qui, pour ceux qui les utilisent, facilite l'intégration et l'utilisation de l'outil. L'outil est aussi disponible via son interface en ligne de commande qui est d'ailleurs la principale méthode.

L'outil est désigné de façon à être personnalisable et extensible, il s'assure que les tâches non modifiées ne soient pas ré-exécutées inutilement à l'aide de ses fonctionnalités (Tel que 'Incremental Builds' et 'Task Output Caching').

Gradle est fondée sur une JVM ce qui simplifie radicalement les problèmes de compatibilité. Il est cependant nécessaire d'installer 'JDK' pour l'utilisé. Cependant, l'outil n'est pas limité qu'à des projets JVM. D'ailleurs, Gradle implémente des conventions afin de simplifier les scripts. L'idée de convention peut cependant apporté des doutes tant qu'aux limites possibles imposée par celles-ci - Problème que Gradle évite en permettant à l'utilisateur d'écraser ces conventions et de personnaliser ses tâches.

Gradle offre même un guide permettant de migrer de 'Maven' à 'Gradle' !

## Description de Maven

Maven est un mot "Yiddish" voulant dire "accumulation de connaissance" mais aussi un outil de gestion de build permettant de simplifier les projets Java. Cet outil fut initialement créé pour simplifier les builds du projet "Jakarta Turbine"

Maven présente cinq objectifs majeurs :
- Simplifier les processus de build
- Offrir un système de build uniforme
- Offrir des informations de projet de qualité
- Offrir des lignes directive pour les meilleurs pratique de développement
- Permettre une migration transparente vers de nouvelles fonctionnalité

On remarque donc facilement que le mot d'ordre de Maven est la "Qualité" ce qui en fait un outil intéressant pour les groupe de développement structure.

Pour Maven chaque projet est configuré selon le POM (Project Object Model) contenant toutes les informations nécessaires tel que le nom, version, dépendance, etc...

## Critere d'evaluation

- Performance
- Flexibilité
- Documentation

# Maven vs Graddle

Nous allons detailler la comparaison de Maven et Graddle

## Performance

La performance d'un outil fait sans doute partie des critères les plus importants. Un outil plus rapide dans l'exécution des demandes sauve du temps, et nous savons que le temps est de l'argent.

Dans un outil de build la performance peut se traduire, entre autre, par la rapidité à effectuer des tests, à compiler une première fois et à compiler après des modifications. Gradle se distingue positivement de Maven pour ces aspects. En effet, à l'aide de plusieurs stratégies que Maven ne possède pas, pour des projets identique, des tests de performances prouve que Gradle est au moins deux fois plus 'rapide' que Maven dans l'ensemble des scénarios possibles.

### La compilation incrémentée

Utilisée par Gradle et non par Maven, la compilation incrémentée permet à l'outil d'analyser les dépendances et de recompiler 'seulement' celles qui sont affectées par les modifications apportées au projet. De cette façon, l'outil ne compile pas inutilement ce qui n'a pas été modifié et sauve par le fait même des ressources CPU.

### Le daemon Gradle et la cache

Le daemon Gradle est un processus en arrière-plan qui garde l'information des builds en cache. De cette façon, Gradle utilise un feature appelé 'Build Cache' (Par la commande --build-cache) qui stock soit localement ou en ligne les résultats d'un build. Cette exécution permet aux prochains builds d'aller chercher dans cette mémoire cache les résultats de builds précédent et de les comparer aux entrées du build présent. Cette stratégie permet d'éviter de "re-build" des informations inchangés.

Ainsi, comme détaillé ci-dessus, la performance fait partie des critères important de notre projet puisque les configurations de chacun varient et peuvent impacter la vitesse de build des projets. Ainsi, d'après nos recherches, nous somme d'accord sur le fait que Gradle semble être considérablement plus rapide que Maven selon les projets et les configurations.

## Documentation

Nous avons décidé de considérer la documentation de chaque outils comme critère majeur de notre projet puisque nous découvrons de tels outils. Il est donc extrêmement important pour nous de maitriser la documentation disponible et accessible pour le déroulement du projet.

Notre première impression sur les documentations disponibles sur les sites internets respectifs de chaque outils fut relativement unanime. La navigation dans la documentation de Graddle nous semblait plus naturelle et intuitive comparément a celle de Maven.

De plus l'esthétique même de la documentation fut un critère de sélection. Nous savons pertinemment que nous allions finir par passer beaucoup de temps sur cette page et donc une documentation visuellement attrayante était plus intéressante.

Pour Graddle, le [site internet](https://docs.gradle.org/current/userguide/getting_started.html) nous offre directement la possibilité de télécharger la documentation complète du projet au format PDF, ce qui nous a pas été proposé sur Maven, ou du moins, qui ne nous est pas offert aussi simplement pour Maven.

Pour conclure, l'équipe de développement du projet considère la documentation de Graddle plus accessible sur le site internet. La navigation dans les différentes parties est plus intuitive et visuellement plus attrayante que celle de Maven.

## Flexibilité

Notre troisième critère est la flexibilité. La flexibilité appliquée au domaine de l’informatique est la capacité à un logiciel d’évoluer et de se plier aux demandes de l’utilisateur en fonction des projets qu’il entreprend.

Celle-ci nous permet des ajustements et des changements au cours d’un processus sans toutefois rendre son utilisation plus complexe ou difficile. Ce critère est très important pour nous car en développement de logiciel, les projets sont généralement de longue haleine. Il arrive souvent des situations ou l’ampleur d’un projet peut changer, s’étendre ou se modifier.

Selon notre comparaison, Gradle nous semble être meilleur en termes de flexibilité pour plusieurs raisons. Comme mentionné plus haut, Gradle fait preuve d'une grande extensibilité. C'est-à-dire qu'il est facile d'étendre Gradle afin d'offrir nos propres types de tâches ou des modèles de build. Par exemple, un support accru est disponible pour Android. Gradle n'est aussi pas limité par XML pour décrire les builds.

Au contraire, Gradle permet la déclaration de domaines de problèmes en domain-specific-language implémenté en Groovy plutôt qu'XML. Il est donc possible d'écrire une logique sur mesure en Java, puisque c'est le langage que nous avons travaillé le plus jusqu'à maintenant. Selon nous, Gradle s'est inspiré des autres outils de build et a su prendre les avantages de chaque afin d'offrir un produit supérieur au niveau, entre autre, de la flexibilité.

Ainsi, comme nous l'avons souvent exprimé dans ce rapport, nous cherchons à optimiser au maximum nos performances dans ce projet tout en maximisant notre capital d'apprentissage. Une forte flexibilité dans un outil de build va nous permettre d'ajuster notre projet au fil du temps et des nouvelles découvertes sur cet outil.

## Conclusion

Comme détaillé dans les trois points précédent nous avons décidé d'utiliser l'outil de build Graddle pour le projet dans l'objectif de maitriser cet outil au fil de la session afin de pouvoir l'implémenter dans nos prochains projets de programmation Java.
